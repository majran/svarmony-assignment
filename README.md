# Task 1
## Run code on a machine with docker

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker build -t microservice-tester .
```

2. run `short test`

```bash
docker run microservice-tester short
```

3. run `long test`

```bash
docker run microservice-tester long
```

2. run `short and long test`

```bash
docker run microservice-tester
```

# Task 2

## Question 1. Do you have any additional question for the computer vision team that could help us to plan the migration?

Yes, there are a few additional questions we should ask the computer vision team:

 - How often do the model files change, and how do they deploy new models?
 - Is it possible to have multiple instances of each component running simultaneously for redundancy and/or load balancing?
 - Can the mobile app tolerate some latency in the response, or does it require an immediate response?
 - Is it acceptable to discard some image requests if the system is overloaded, or do we need to ensure that every request is processed?

## Question 2. Suppose your questions are answered to your satisfaction, how would you assemble these components in the cloud? Which tools would you use?

We would likely use a combination of cloud computing services to assemble these components in the cloud. Here is a possible architecture:

Mobile app sends images to a load balancer in a serverless computing service like AWS Lambda or Azure Functions.
The load balancer routes the requests to multiple instances of the Preclassifier running in a container orchestration service like Kubernetes or Amazon ECS. Each instance of Preclassifier processes one image at a time and writes the results to a database or message queue like Amazon SQS or Apache Kafka.
The Classifier instances, also running in a container orchestration service, read the image paths and prefilter data from the message queue, classify the images, and write the results to a database or message queue.
The mobile app retrieves the classification results from the database or message queue.

We could use various cloud services for these components, such as Amazon ECR or Docker Hub for container image storage, Amazon S3 or Google Cloud Storage for object storage, and Amazon RDS or Azure Cosmos DB for databases.

To handle the model file updates, we could use a continuous integration/continuous deployment (CI/CD) pipeline to automatically build and deploy new container images with the latest models.

## Bonus: The system is also supposed to be set up in a private cloud that is not connected to the internet (mobile app will send images over local network). Let's assume that any available technology can be set up in this cloud. How shall we proceed?

If the system is to be set up in a private cloud, we would need to use a private network with no public internet access. We could use a Virtual Private Cloud (VPC) in AWS or a Virtual Network in Azure to set up a private network. The mobile app would need to be configured to connect to this private network, which would require some network configuration on the mobile devices.

For the backend components, we could use container orchestration tools like Kubernetes or Docker Swarm to manage the deployment of the Preclassifier and Classifier containers. We would need to set up a private container registry to store the container images, which could be done with tools like Harbor or JFrog Artifactory.

## Bonus: Let's assume that we have usage peaks (10,000 requests/s) that are going beyond what our system can process. How can we design it in a way that the service is still acceptable (200 requests/s, short latency)?

One approach to handling usage peaks beyond what the system can process would be to use a combination of auto-scaling and caching.

Auto-scaling could be used to automatically spin up additional instances of the Preclassifier and Classifier containers to handle the increased load. This could be done using a container orchestration service like Kubernetes, which can automatically scale the number of container instances based on metrics like CPU utilization or queue length.

To reduce the load on the Classifier instances, we could use a caching layer to store the results of recent classifications. This could be done using a distributed in-memory cache like Redis or Memcached.