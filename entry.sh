#!/bin/sh

set -e

orig=$@
cmd=$1
shift

if [ $cmd = 'short' ]; then
  echo 'Test short...'
  exec python test-short.py
elif [ $cmd = 'long' ]; then
  echo 'Test long...'
  exec python test-long.py
elif [ $cmd = 'all' ]; then
  echo 'Test all...'
  exec python test-short.py && python test-long.py
fi

exec "$orig"