import requests
import json
# URL for the endpoint
url = "https://ionaapp.com/assignment-magic/dk/long/"

# List of all possible three-alphanumeric combinations
params = [f"{c1}{c2}{c3}" for c1 in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" for c2 in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" for c3 in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"]

# Iterate through all the parameters and check if the response is valid
for param in params:
    response = requests.get(url + param)
    try:
        uid = json.loads(response.text)["uid"]
        if len(uid) == 32 and uid.isalnum():
            print(f"Parameter {param} is valid")
        else:
            print(f"Parameter {param} is invalid")
    except:
        print(f"Parameter {param} is invalid")
